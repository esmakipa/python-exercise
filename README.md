## Yleistä

Ohjelmoinnin alkeet kevät 2020 -kurssin harjoitustyö.

Harjoitustyössä tehtävänä oli analysoida argonin fotoionisaatiospektriä. Ohjelman osaa ladata datan tiedostoista (simuloitua dataa mittauksesta), piirtää sen näkyviin, muokata sitä ja laskea siitä kiinnostavia arvoja. 

## Toiminnot

- graafinen käyttöliittymä, josta käyttäjä voi valita ohjelman eri toiminnot

- datan lataaminen tiedostoista ohjelman muistiin

- kuvaajan piirtäminen muistissa olevan datan perusteella

- lineaarisen taustan poistaminen spektristä

- spektristä löytyvien piikkien intensiteettien laskenta

- kuvaajan tallentaminen
