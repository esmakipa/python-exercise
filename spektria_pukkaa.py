"""
Ohjelmoinnin alkeet kevät 2020, harjoitustyö

@author Esa Mäkipää

"""

import os
import numpy as np
import ikkunasto as ik

# Ohjelman tila
tila = {
    'tekstilaatikko': None,
    'kuvaajakehys': None,
    'piirtoalue': None,
    'kuvaaja': None,
    'lataanappi': None,
    'piirranappi': None,
    'poistanappi': None,
    'laskenappi': None,
    'tallennanappi': None,
    'lopetusnappi': None,
    'pisteet': [],
    'ladatut_tiedostot': 0
}

# Ohjelman toiminnot
toiminto = {
    'data_ladattu': False,
    'data_piirretty': False,
    'lineaarinen_tausta_poistettu': False,
    'intesiteetit_laskettu': False,
    'kuvaaja_tallennettu': False
}


# Listat x- ja y-akselien datalle
xy_data = {
    'kineettiset_energiat': [],
    'summa_intensiteettispektrit': [],
    'suora': []
}

# Suoran parametrit
suoran_parametrit = {
    'kulmakerroin': 0.0,
    'vakiotermi': 0.0
}

# Tiedoston nimen alkuosa
TIEDALKU = 'measurement_'
# Tiedostopääte
TIEDLOPPU = '.txt'

# Listat datan väliaikaseen tallentamiseen tiedostosta luettaessa
valiaikainen_kineettiset = []
valiaikainen_summa = []

# Viallisten tiedostojen lista
vialliset_tiedostot = []

def tarkista_tiedostonimi(tiedosto):

    """
    Tutkii annetun tiedoston nimen. Tiedoston nimen tulee olla muotoa
    "measurement_x.txt" ("x" on mittauskerran järjestysnumero).
    Palauttaa  kutsujalle totuusarvon.

    :param str tiedosto: tutkittavan tiedoston nimi
    :return palauttaa totuusarvon (True/False) tiedoston nimen oikeellisuudesta
    """

    try:
        # Tallennetaan mittauskerran järjestysnumero
        tiedosto_numero = tiedosto[(tiedosto.find('_') + 1):tiedosto.find('.')]
        #Tutkitaan, onko saatu tulos kokonaisluku
        isinstance(int(tiedosto_numero), int)
    except ValueError:
        # Tiedostonimi ei sisällä järjestysnumeroa, palautetaan False
        return False
    else:
        # Tiedoston nimessä on oikeanlainen järjestysnumero. Tutkitaan, onko
        # tiedoston nimessä sallitut alku- ja loppuosat
        if tiedosto.startswith(TIEDALKU) and tiedosto.endswith(TIEDLOPPU):
            # Tiedostonimi on määrätyn muotoinen: "measurement_x.txt",
            # palautetaan True
            return True
        else:
            # Tiedostonimi ei ole määrätyn muotoinen, palautetaan False
            return False

def lue_data(kansio):

    """
    Lukee kaikki datatiedostot annetusta kansiosta. Ohittaa tiedostot, jotka eivät
    ole muotoa "measurement_x.txt" ("x" on mittauskerran järjestysnumero).
    Ladattujen tiedostojen lukumäärä ilmoitetaan käyttäjälle.

    :param str kansio: kansio/hakemisto, josta datatiedostoja ladataan
    :return: palauttaa ladattujen tiedostojen lukumäärän
    """

    # Nollataan ladattujen tiedostojen lukumäärä
    tila['ladatut_tiedostot'] = 0

    # Asetetaan ohjelman toimintojen tila
    toiminto['data_ladattu'] = False
    toiminto['data_piirretty'] = False
    toiminto['lineaarinen_tausta_poistettu'] = False
    toiminto['intesiteetit_laskettu'] = False
    toiminto['kuvaaja_tallennettu'] = False

    # Tyhjennetään hyväksytyn datan ja viallisten tiedostojen nimien listat
    xy_data['kineettiset_energiat'].clear()
    xy_data['summa_intensiteettispektrit'].clear()
    vialliset_tiedostot.clear()

    # Luetaan annetun kansion sisältö
    kansion_sisalto = os.listdir(kansio)
    # Kansiossa olevien tiedostojen läpikäynti
    for nimi in kansion_sisalto:

        # Tarkastetaan tutkittavan tiedoston nimi, vain muotoa 'measurement_x.txt'
        # olevat tiedostot hyväksytään käsiteltäväksi
        if tarkista_tiedostonimi(nimi):
            # Tiedoston polku: annettu kansio + kansiossa olevan tiedoston nimi
            polku = os.path.join(kansio, nimi)
            # Tyhjennetään väliaikaiset listat ennen tiedoston avaamista
            valiaikainen_kineettiset.clear()
            valiaikainen_summa.clear()
            # Asetetaan tiedot_luettu _oikein arvoksi True
            tiedot_luettu_oikein = True

            # Avataan tiedosto lukua varten
            with open(polku, 'r') as tiedosto:
                # Käydään tiedostosta luetut rivit läpi rivi kerrallaan
                for rivi in tiedosto:
                    # Luetaan rivin sisältö monikkoon data
                    data = rivi.split(' ')

                    # Datan tulee sisältää 2 arvoa:
                    # data[0] (liukuluku)
                    # data[1] (liukuluku)
                    if len(data) == 2:
                        try:
                            # Liukulukujen tarkistus
                            float(data[0].strip())
                            float(data[1].strip())
                        except ValueError:
                            # Tiedoston tiedoissa on virhe, asetetaan tiedot_luettu_oikein
                            # arvoksi False ja lisätään tiedoston nimi viallisten tiedostojen
                            # listalle
                            tiedot_luettu_oikein = False
                            vialliset_tiedostot.append(nimi)
                            # Nyt käsittelyssä olevan tiedoston läpikäynti voidaan
                            # keskeyttää ja siirtyä seuraavaan tiedostoon
                            break
                        else:
                            # Tiedoston rivin tiedot ovat oikeat
                            # Luetaan x-akselin (kineettiset energiat) arvot vain kerran
                            # ja lisätään ne väliaikaiselle listalle
                            if not xy_data['kineettiset_energiat']:
                                valiaikainen_kineettiset.append(float(data[0].strip()))
                            # Lisätään y-akselin (intensiteettispektri) arvot väliaikaiselle
                            # listalle
                            valiaikainen_summa.append(float(data[1].strip()))
                    else:
                        # Tiedoston rivillä ei ole oikea määrä dataa, asetetaan
                        # tiedot_luettu_oikein arvoksi False ja lisätään tiedoston
                        # nimiviallisten tiedostojen listalle
                        tiedot_luettu_oikein = False
                        vialliset_tiedostot.append(nimi)
                        # Nyt käsittelyssä olevan tiedoston läpikäynti voidaan
                        # keskeyttää ja siirtyä seuraavaan tiedostoon
                        break
            if tiedot_luettu_oikein:
                # Kaikki tiedoston tiedot sallittuja ja luettu oikein
                # Lisätään kineettisen energian arvot listaan (vain kerran)
                if not xy_data['kineettiset_energiat']:
                    xy_data['kineettiset_energiat'].extend(valiaikainen_kineettiset)
                # Lisätään ensimmäisen tiedoston intensiteettispektrien arvot listaan
                if len(xy_data['summa_intensiteettispektrit']) == 0:
                    xy_data['summa_intensiteettispektrit'].extend(valiaikainen_summa)
                # Summataan intensiteettispektrien arvot
                else:
                    xy_data['summa_intensiteettispektrit'] = \
                    [xy_data['summa_intensiteettispektrit'][i] + \
                    valiaikainen_summa[i] for i in range(len(valiaikainen_summa))]
                # Kasvatetaan ladattujen tiedostojen arvoa
                tila['ladatut_tiedostot'] += 1
                # Tietojen lataaminen on suoritettu, tiedostojen sisältämä data on ladattu
                toiminto['data_ladattu'] = True

                # Tyhjennetään piirtoalue ja kuvaaja sekä valitut pisteet, jos ovat jo olemassa
                if tila['ladatut_tiedostot']:
                    tila['kuvaaja'].clf()
                    tila['piirtoalue'].draw()
                    tila['pisteet'].clear()
        else:
            # Tiedoston nimi ei ole kelvollinen, lisätään tiedosto nimi viallisten
            # tiedostojen listalle
            vialliset_tiedostot.append(nimi)

    # Palautetaan ladattujen tiedostojen lukumäärä
    return tila['ladatut_tiedostot'], len(vialliset_tiedostot)

def piirra_data():

    """
    Piirtää kuvaajan ohjelmassa olevan datan perusteella.
    """

    # Tyhjennetään tekstilaatikko
    ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], "", tyhjaa=True)

    # Tutkitaan, onko mittaustuloksia ladattu
    # Jos mittaustulokset on ladattu ohjelmaan, kuvaaja voidaan piirtää
    if tila['ladatut_tiedostot'] and toiminto['data_ladattu']:
        # Tyhjennetään kuvaaja ennen uuden piirtämistä
        tila['kuvaaja'].clf()
        # Piirretään uusi kuvaaja ohjelmassa olevan datan perusteella
        tila['kuvaaja'].add_subplot().plot(xy_data['kineettiset_energiat'], \
        xy_data['summa_intensiteettispektrit'])
        # Asetetaan otsikot kuvaajan akseleille
        tila['kuvaaja'].add_subplot().set_xlabel('Binding energy (eV)')
        tila['kuvaaja'].add_subplot().set_ylabel('Intensity (arbitrary units)')
        # Näytetään piirretty kuvaaja
        tila['piirtoalue'].draw()
        # Kuvaajan piirtäminen on suoritettu
        toiminto['data_piirretty'] = True
        # Jos kuvaaja on piirretty eikä muita toimintoja ole suoritettu,
        # tulostetaan viesti valita kuvaajan laskevalta suoralta pisteet
        # lineaarisen taustan poistamiseksi
        if toiminto['data_piirretty']:
            ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
            "Valitse pisteet laskevalta suoralta kuvaajan alku- ja loppupäästä", tyhjaa=True)
        # Jos kuvaaja on piirretty ja lineaarinen tausta on poistettu, tulostetaan
        # viesti valita pisteet kuvaajan piikkien intensiteetin laskemiseksi
        if toiminto['data_piirretty'] and toiminto['lineaarinen_tausta_poistettu']:
            ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
            "Valitse pisteet (4 kpl) piikkien intensiteettien laskemiseksi", tyhjaa=True)
    else:
        # Mittaustuloksia ei ole vielä ladattu, tulostetaan ilmoitus
        # viesti-ikkunassa
        ik.avaa_viesti_ikkuna('Huom!', 'Mittaustuloksia ei ole vielä ladattu.')

def valitse_datapiste(event):

    """
    Ottaa vastaan hiiren klikkaustapahtuman ja lukee siitä datapisteen x- ja
    y-arvot. Arvot tulostetaan tekstilaatikkoon sekä talletetaan ohjelman
    tila-sanakirjassa olevaan pisteet-listaan.

    :param event hiiren klikkaustapahtuma
    """

    # Lineaarisen taustan poisto, valitaan kaksi pistettä
    if len(tila['pisteet']) == 2 and toiminto['data_piirretty'] and \
    not toiminto['lineaarinen_tausta_poistettu']:
        tila['pisteet'].clear()
        ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
        "Valitse pisteet laskevalta suoralta kuvaajan alku- ja loppupäästä", tyhjaa=True)
    # Kuvaajan piikkien intensitettien laskenta, valitaan neljä pistettä
    if len(tila['pisteet']) == 4 and toiminto['data_piirretty'] and \
    toiminto['lineaarinen_tausta_poistettu']:
        tila['pisteet'].clear()
        ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
        "Valitse pisteet (4 kpl) piikkien intensiteettien laskemiseksi.", tyhjaa=True)

    # Lisätään hiirellä valitun pisteen koordinaatit listaan
    tila["pisteet"].append([event.xdata, event.ydata])
    # Tulostetaan tekstilaatikkoon valitun pisteen koordinaatit
    ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
    "Valittu piste ({x:.2f}, {y:.2f})".format(x=event.xdata, y=event.ydata), tyhjaa=False)

def laske_parametrit():

    """
    Laskee suoran parametrit (kulmakerroin ja vakiotermi) valittujen pisteiden
    x- ja y-arvojen perusteella. Parametrit tallennetaan ohjelman
    suoren_parametrit-sanakirjaan.

    :param event hiiren klikkaustapahtuma
    """

    #Piste 1
    x1, y1 = tila['pisteet'][0]
    #Piste 2
    x2, y2 = tila['pisteet'][1]

    # Tallennetaan valittujen pisteiden perusteella lasketun suoran parametrit
    suoran_parametrit['kulmakerroin'] = (y2 - y1) / (x2 - x1)
    suoran_parametrit['vakiotermi'] = ((x2 * y1) - (x1 * y2)) / (x2 - x1)

def on_sallitut_pisteet():

    """
    Tarkistaa, ovatko valitut pisteet kelvollisia.

    :return palauttaa totuusarvon (True/False) valittujen pisteiden kelvollisuudesta
    """

    #Piste 1
    x1, y1 = tila['pisteet'][0]
    #Piste 2
    x2, y2 = tila['pisteet'][1]

    # Valitut pisteet ovat samat, palautetaan False
    if (x1 == x2) and (y1 == y2):
        return False
    # Valittujen pisteiden x-arvot ovat samat (suora on pystysuora),
    # palautetaan False
    elif x1 == x2:
        return False
    # Pisteet ovat kelvolliset ja suora voidaan määrittää
    else:
        # Lasketaan suoran parametrit ja palautetaan True
        laske_parametrit()
        return True

def laske_suoran_pisteet():

    """
    Laskee suoran pisteet.
    """

    for x in xy_data['kineettiset_energiat']:
        xy_data['suora'].append(x * suoran_parametrit['kulmakerroin'] + \
                                suoran_parametrit['vakiotermi'])

def poista_tausta():

    """
    Poistaa lineaarisen taustan kuvaajasta. Kuvaajan arvoista vähennetään
    määritellyn suoran arvot.
    """

    # Tutkitaan, onko mittaustuloksia ladattu ohjelmaan
    if tila['ladatut_tiedostot'] and toiminto['data_ladattu']:
        # Tutkitaan, onko kuvaaja piirretty
        if toiminto['data_piirretty']:
        #while True:
            # Lineaarista taustaa ei vielä ole poistettu, se voidaan poistaa
            if not toiminto['lineaarinen_tausta_poistettu']:
                if  len(tila['pisteet']) == 2 and on_sallitut_pisteet():
                    # Lasketaan valitun suoran pisteet
                    laske_suoran_pisteet()
                    # Vähennetään kuvaajasta suoran arvot
                    xy_data['summa_intensiteettispektrit'] = \
                    [xy_data['summa_intensiteettispektrit'][i] - \
                    xy_data['suora'][i] for i in range(len(xy_data['kineettiset_energiat']))]
                    # Piirretään kuvaaja uuden tiedon perusteella
                    piirra_data()
                    # Tyhjennetään valittujen pisteiden lista
                    tila['pisteet'].clear()
                    # Tulostetaan viesti valita pisteet piikkien intensiteettien laskemiseksi
                    ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
                    "Valitse pisteet (4 kpl) piikkien intensiteettien laskemiseksi.", tyhjaa=True)
                    # Asetetaan lineaarisen taustan poistava toiminto suoritetuksi
                    toiminto['lineaarinen_tausta_poistettu'] = True
                else:
                    # Suoran määrittäviä pisteitä ei ole vielä valittu tai niitä
                    # on valittu vasta yksi kappale
                    if len(tila['pisteet']) == 0 or len(tila['pisteet']) == 1:
                        ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
                        "Valitse pisteet laskevalta suoralta kuvaajan alku- ja loppupäästä.", tyhjaa=True)
                    # Suoran määrittämiseksi valitut pisteet eivät ole kelvollisia
                    if len(tila['pisteet']) == 2:
                        ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
                        "Valitut pisteet eivät olleet kelvollisia.", tyhjaa=True)
                        tila['pisteet'].clear()
            else:
                # Lineaarinen taustan poistava toiminto on jo suoritettu
                ik.avaa_viesti_ikkuna('Huom!', 'Lineaarinen tausta on jo poistettu.')
        else:
            # Kuvaajaa ei vielä ole piirretty
            ik.avaa_viesti_ikkuna('Huom!', 'Dataa ei ole vielä piirretty.')
    else:
        # Mittaustuloksia ei ole vielä ladattu, tulostetaan ilmoitus
        # viesti-ikkunassa
        ik.avaa_viesti_ikkuna('Huom!', 'Mittaustuloksia ei ole vielä ladattu.')

def lahin(arvo):

    """
    Määrittää käyttäjän valitseman pisteen lähimmän kuvaajadatan x-arvon indeksin.

    :param arvo: arvo, jolle haetaan lähin kuvaajan x-arvon indeksi.
    :return lähimmän x-arvon indeksin valitulle pisteelle
    """

    # Muodostetaan lista itseisarvoista (kineettiset_energiat x-arvo - valitun
    # pisteen x-arvo)
    lista = [abs(xy_data['kineettiset_energiat'][i] - \
    float(arvo)) for i in range(len(xy_data['kineettiset_energiat']))]

    # Palautetaan pienimmän itseisarvon omaava indeksi
    return lista.index(min(lista))

def laske_intensiteetit():

    """
    Laskee kuvaajasta piikkien intensiteetit käyttäjän valitsemien pisteiden mukaan.
    """

    # Tutkitaan, onko mittaustuloksia ladattu ohjelmaan
    if tila['ladatut_tiedostot'] and toiminto['data_ladattu']:

        # Tutkitaan, onko kuvaaja piirretty ja lineaarinen tausta poistettu
        if toiminto['data_piirretty'] and toiminto['lineaarinen_tausta_poistettu']:

            if len(tila['pisteet']) == 4:

                #Järjestetään valitut pisteet suuruusjärjestykseen
                tila['pisteet'] = sorted(tila['pisteet'], key=lambda k: [k[0]])

                # Piikki 1, alku- ja loppupisteen x-arvojen indeksit
                alkupiste1 = lahin(tila['pisteet'][0][0])
                loppupiste1 = lahin(tila['pisteet'][1][0])
                #Määritetään pisteiden väliset y-arvot
                y1 = np.array(xy_data['summa_intensiteettispektrit'][alkupiste1:loppupiste1])

                # Piikki 2, alku- ja loppupisteen x-arvojen indeksit
                alkupiste2 = lahin(tila['pisteet'][2][0])
                loppupiste2 = lahin(tila['pisteet'][3][0])
                # Määritetään pisteiden väliset y-arvot
                y2 = np.array(xy_data['summa_intensiteettispektrit'][alkupiste2:loppupiste2])

                # Lasketaan piikkien intensiteetit
                intensiteetti1 = np.trapz(y1)
                intensiteetti2 = np.trapz(y2)

                # Tulostetaan laskennan tulokset tekstilaatikkoon
                ik.kirjoita_tekstilaatikkoon(tila['tekstilaatikko'], \
                'Korkeamman piikin intensiteetti: {piikki:.4f}'.format(piikki=float(intensiteetti1)), tyhjaa=True)
                ik.kirjoita_tekstilaatikkoon(tila['tekstilaatikko'], \
                'Matalamman piikin intensiteetti: {piikki:.4f}'.format(piikki=float(intensiteetti2)), tyhjaa=False)
                # Tyhjennetään valittujen pisteiden lista
                tila['pisteet'].clear()

            else:
                # Pisteitä ei ole vielä valittu vaadittua määrää
                ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
                "Valitse pisteet (4 kpl) piikkien intensiteettien laskemiseksi.", tyhjaa=True)
                tila['pisteet'].clear()
        else:
            # Kuvaajaa ei ole vielä piirretty ja/tai lineaarista taustaa ei ole vielä poistettu
            ik.avaa_viesti_ikkuna('Huom!', \
            'Dataa ei ole vielä piirretty ja/tai lineaarista taustaa ei ole poistettu.')
    else:
        # Mittaustuloksia ei ole vielä ladattu ohjelmaan
        ik.avaa_viesti_ikkuna('Huom!', 'Mittaustuloksia ei ole vielä ladattu.')

def tallenna_kuvaaja():

    """
    Tallentaa kuvaajan ohjelman muistissa tallennushetkellä olevasta datasta.
    Jos dataa (mittaustuloksia) ei ole vielä ladattu ohjelmaan muistiin, näytetään
    käyttäjälle viesti-ikkuna. Pyytää käyttäjää valitsemaan tallennuskansion ja
    antamaan tiedoston nimen.
    """
    # Tutkitaan, onko mittaustuloksia ladattu ohjelmaan
    if tila['ladatut_tiedostot'] and toiminto['data_ladattu']:
        # Data on ladattu ja kuvaaja voidaan piirtää ohjelman muistissa olevasta datasta
        # ja tallentaa käyttäjän antamaan hakemistoon valitsemallaan nimellä

        # Avataan tallennusikkuna
        polku = ik.avaa_tallennusikkuna('Tallenna kuvaaja')
        # Muodostaa kuvaajan ja lisää akseleiden otsikot
        tila['kuvaaja'].add_subplot().plot(xy_data['kineettiset_energiat'], \
                                           xy_data['summa_intensiteettispektrit'])
        tila['kuvaaja'].add_subplot().set_xlabel('Binding energy (eV)')
        tila['kuvaaja'].add_subplot().set_ylabel('Intensity (arbitrary units)')
        # Tallennetaan kuvaaja valittuun hakemistoon ja valitulla nimellä
        tila['kuvaaja'].savefig(polku)
        # Tulostetaan tekstilaatikkoon viesti kuvan tallentamisesta
        ik.kirjoita_tekstilaatikkoon(tila["tekstilaatikko"], \
        "Kuvaaja tallennettu", tyhjaa=True)
        # Asetetaan kuvaajan tallennustoiminto suoritetuksi
        toiminto['kuvaaja_tallennettu'] = True

    else:
        # Mittaustuloksia ei ole vielä ladattu ohjelmaan
        ik.avaa_viesti_ikkuna('Huom!', 'Mittaustuloksia ei ole vielä ladattu.')

def avaa_kansio():

    """
    Napinkäsittelijä, joka pyytää käyttäjää valitsemaan kansion avaamalla
    kansioselaimen. Lataa datan valitusta kansiosta ja ilmoittaa käyttöliittymän
    tekstilaatikkoon montako tiedostoa ladattiin. Ilmoittaa myös viallisten
    tiedostojen lukumäärän.
    """

    # Avaa hakemistoikkunan tiedostot sisältävän hakemiston valitsemiseksi
    hakemisto = ik.avaa_hakemistoikkuna('hakemisto')
    # Luetaan data tiedostojen sisältämä data ohjelmaan
    ladatut_tiedostot, vialliset_tiedostot = lue_data(hakemisto)

    # Tulostetaan oikein ladattujen ja viallisten tiedostojen lukumäärä
    if ladatut_tiedostot:
        ik.kirjoita_tekstilaatikkoon(tila['tekstilaatikko'], \
        'Ladattiin yhteensä {tiedosto} tiedosto(a) \
        {rivinvaihto}Viallisten tiedostojen lukumäärä: {vialliset}'\
        .format(tiedosto=(ladatut_tiedostot), rivinvaihto=os.linesep, \
        vialliset=(vialliset_tiedostot)), tyhjaa=True)

def main():

    """
    Luo käyttöliittymän, jossa on painonapit toiminnoille, klikattava kuvaaja
    sekä tekstilaatikko.
    """

    ikkuna = ik.luo_ikkuna("Spektriä pukkaa")
    nappikehys = ik.luo_kehys(ikkuna, ik.VASEN)
    kuvaajakehys = ik.luo_kehys(ikkuna, ik.YLA)
    tekstikehys = ik.luo_kehys(ikkuna, ik.YLA)
    vaakaerotin1 = ik.luo_vaakaerotin(tekstikehys, 20)
    lataanappi = ik.luo_nappi(nappikehys, "lataa data", avaa_kansio)
    piirranappi = ik.luo_nappi(nappikehys, "piirrä data", piirra_data)
    poistanappi = ik.luo_nappi(nappikehys, "poista tausta", poista_tausta)
    laskenappi = ik.luo_nappi(nappikehys, "laske intensiteetit", laske_intensiteetit)
    tallennanappi = ik.luo_nappi(nappikehys, "tallenna kuvaaja", tallenna_kuvaaja)
    vaakaerotin2 = ik.luo_vaakaerotin(nappikehys, 10)
    lopetusnappi = ik.luo_nappi(nappikehys, "quit", ik.lopeta)

    # Luodaan piirtoalue ja kuvaaja
    piirtoalue, kuvaaja = ik.luo_kuvaaja(kuvaajakehys, valitse_datapiste, 600, 400)

    # Luodaan tekstilaatikko ilmoituksille ja tuloksille
    tekstilaatikko = ik.luo_tekstilaatikko(tekstikehys, 80, 10)

    # Tallennetaan sanakirjaan viittaukset käyttöliittymäelementteihin
    tila['tekstilaatikko'] = tekstilaatikko
    tila['kuvaajakehys'] = kuvaajakehys
    tila['piirtoalue'] = piirtoalue
    tila['kuvaaja'] = kuvaaja
    tila['lataanappi'] = lataanappi
    tila['piirranappi'] = piirranappi
    tila['poistanappi'] = poistanappi
    tila['laskenappi'] = laskenappi
    tila['tallennanappi'] = tallennanappi
    tila['lopetusnappi'] = lopetusnappi
    ik.kaynnista()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print('Ohjelma keskeytettiin.')
